# litcoffee-mode -- Literate CoffeeScript major mode

Literate CoffeeScript mode provides support for `.litcoffee` files, using
[coffee-mode](https://gitlab.com/thornjad/coffee-mode).

This major mode is based on work done by syohex in 2014 in
[emacs-literate-coffee-mode](https://github.com/syohex/emacs-literate-coffee-mode).

Copyright 2019 Jade Michael Thornton under the terms of the MIT license
